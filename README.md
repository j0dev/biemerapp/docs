BiemerApp Documentation
======

This repository contains the documentation of the entire project.


## Live documentation
A published version is available at [biemerapp.j0dev.nl](https://biemerapp.j0dev.nl/).


## Documentation format
This documentation is made with [MkDocs](https://www.mkdocs.org/) and [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/), a static website framework for building documentation similar to ReadTheDocs.  
The documentation is build up from simple [MarkDown](https://www.markdownguide.org) files that are easily readable in plain text, but can also be rendered out to other formats, like a webpage or pdf with styling, like headings, lists, etc.


## Rendering the website
To view a local version of the website, you first need to install MkDocs and the plugins used. This guide presumes you already have [python](https://www.python.org/downloads/) installed. Open a terminal / command-prompt and run `pip -r python-requirements.txt`. This will install all required modules.  
After installing MkDocs and the used plugins. run `mkdocs serve`.  
To generate the website content run `mkdocs build`. The website should be output the a directory name `public`.
