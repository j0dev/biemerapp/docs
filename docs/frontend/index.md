# The Slide Deck Frontend
This section covers the inner workings of the client-facing slide-deck.  
This section also contains information on how to customize and/or extend the user-facing slide-deck system outside of the slide management system.
