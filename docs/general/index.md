# General documentation
This section contains the basics of the entire system.  
This section also serves as a general entry-point to the split nature of this project, introducing and guiding you through the sub-projects that make up the BiemerApp system.

If you are already accustomed to or looking for documentation on the specific sub components of the system, please use the navigation tabs at the top.
