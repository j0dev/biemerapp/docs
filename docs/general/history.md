# The Origins
The current _BiemerApp_ is the 3rd major iteration of the presentation / slide-deck system.  
Or 4th, if you count the rewrite of the same system design in after the 2nd iteration broke due to framework updates.

## The first version
It all began a few weeks before an event. We did not have a system to control the information on a projector screen. usually someone build something for that, but as there was a whole new set of crew organizing. I was already building the entire website system for attendees. In a weeks time next to college, I build the most simple system.

The first iteration was a simple 2 slide content panes in an HTML wrapper that faded one in and the other out.  
On the website system, there was a page that indexed a directory of HTML files and spit it out in JSON.  
Using some simple java-script, the 2 slide containers were filled with the data, and after going through the list, it would reload the contents from the JSON endpoint.

A lot was learned from that experience, what we would like, and how bad it performed.
One of the big mistakes was having video's constantly loading and unloading on a pc without any user-interaction, something the web was not build for yet. after a few cycles, video content like Twitch and YouTube would simply stop loading in.


## The second version
This is where the real development started.  
One of the first major design changes was using a purpose build system instead of re-purposing the website. As we wanted more control, we needed a system that would allow this.  
At the time I choose [meteor](https://www.meteor.com/). At the time, one of its major benefits was the synchronization of collections between client and server, that happend automatically, and could be used to trigger changes in the content displayed, real-time.
The first big change was that every slide would stay loaded in DOM (the page). This ensured that content did not have to be replaced and garbage-collected putting more strain on the system then needed.  
Now when switching a slide, it was just a matter of displaying something or not.

Some of the most notable features that came out of this:

- Real-time content updates. Content was always synchronized, meaning a change by and admin is directly reflected on screen, even when the slide is on.
- Synchronized display of slides. Every client displaying the same content at the same time.
- No more loading and unloading constantly from DOM, which broke videos when doing often
- Management dashboard. Previously a shared directory was used with direct html files, now there was an interface and nice editor.


### The rewrite
After the first year / event when trying to update the dependencies and the framework that was used, the whole system started to mis-behave. Not willing to work in production mode anymore, and some things that just didn't work as expected.  
I first tried a clean install of the latest version with the same plugins used before. Unfortunately some were incompatible, not updated, or simply just not working anymore. After seeing that for some specific functionality and way of working there were no alternatives, I got fed up and decided to rewrite with no frameworks.

I chose to use established and well known 3th party libraries that took care of what was needed to build a similar system. And this was the first time I also split out the backend and control system. (the frontend was still embedded in the backend for the most part)

Other then some small incremental improvements, no major design or feature changes were made.


## The current iteration
In design the current iteration is not that far apart from the 2nd iteration. The only real difference between then is the worked out feature-set (and how that's made available), and the fact that the system now uses [reveal.js](https://revealjs.com/) to do the actual slide transitions and management. Previously it used an all custom self build system.  
The principles of the system and major features are still the exact same. It is just that we use different technology now to do the same. Use Typescript, use an already established slide-deck frontend to do the heavy lifting of switching, and providing building blocks with all sorts of features.

One feature that is now to be included as first citizen, is the ability for attendees to open the presentation themselves. Even though some effort was already made in the previous iteration, due to a lack of authentication or issues therewith. And proper validation that it was good enough for visitors. It was never introduced to them in that way because of the shortcomings.
