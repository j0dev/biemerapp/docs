# Frequently Asked Questions

### Whats up with the name?
> The _biemer_ section of _BiemerApp_ is a phonetic play on the word _beamer_ (a European synonym for _projector_).

> the system in the beginning was always referred to as _presentation app / system_, but was sometimes also named _BeamerApp_.  
At some point joking a bit around it was coined to use the name _BiemerApp_. Specifically using the misspelling / play on the word. And so it was made official that the system from that point on was referred to as _BiemerApp_
