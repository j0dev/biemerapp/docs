---
template: home.html
---
<style>
  .md-typeset h1 {
    font-size: 3em;
    margin-bottom: 0;
  }
  .md-typeset h2 {
    font-size: 2em;
    margin-top: 0;
  }
</style>

<h1>BiemerApp</h1>
<h2>A media-screen slide-deck system</h2>
