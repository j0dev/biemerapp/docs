# Biemer Backend
This section covers everything on the backend system.

The backend provides the API-endpoints for controlling the actual content served to clients. As well as the synchronization system behind the live slide-deck.
