# Biemer Control
This section covers everything on the control web-app.

Biemer control is the graphical user-interface to control the backend. It allows creating, managing and scheduling the slide-deck. All easily controllable through the Biemer Control web-app. Even remote control of dedicated media screens is build in.
