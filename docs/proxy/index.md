# Biemer Proxy
This section contains the documentation on the proxy component. The glue in the system bringing all lose components together under _one_ central entry-point / domain.
